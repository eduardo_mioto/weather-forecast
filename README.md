
# Weather Forecast ![coverage](https://img.shields.io/badge/coverage-87.2%25-green.svg?maxAge=2592000)  ![version](https://img.shields.io/badge/version-1.0.0-blue.svg?maxAge=2592000)

### Getting Started

##### Installation

Make sure you have the Java JDK on your machine. If you do not have it download it and add it to the Path of your machine

```sh
$ javac -version
```

Make sure you own the Maven on your machine. If you do not have it download it and add it to the Path of your machine
After installation run the command below to ensure that it is properly installed

```sh
$ mvn -v
```

##### Compilation

* Run the command below to download the dependencies and generate the executable jar

```sh
$ mvn clean install
```

##### Analisys

```sh
$ mvn clean install sonar:sonar
```

##### Execution
* Get the executable jar in the 'target' folder
* Run the command below to start the program

```sh
$ java -jar infinitec-weather-forecast.jar
```

##### Using
* To use the project, you may import the collection `Infinitec - Weather Forecast.postman_collection.json` on Postman to execute the endpoint for weather forecast verification. This collection can be found at `src/main/resources/getting-started`

---
### Technical Perspective

##### Choose between JSONObject and GSON
`JSONObject` was used instead of a serializable GSON motivated by the intention to reduce the unuseful code, considering that and JSON is being read and treated to resolve a couple  of fields. If the GSON was used, the entire JSON should be serialized, which in a high throuput scenario, may means an bad use of computational resources.

##### Undertow instead of Tomcat or Jetty
`Undertow` is more reliable and faster than Jetty and Tomcat. It can be observed in several performance tests that can be found around the internet.

##### StringBuilder usage instead of String concatenation
Even considering that JIT operates and optimization on String concatenation it's just valid for a few consecutives concatenations, that why it was used the `StringBuilder` for a better memory and CPU usage.

##### Logback/SLF4J instead of Log4J
The `Logback/SLF4J` was chosen because it offers a better approach to populate a log entry with variable, considering the wildcard strategy implemented on this framework. 

##### Healthcheck Implementation
Considering a cloud-ready system, it's always necessary an health-check endpoint implementation, to be possible to be reached by a Load Balancer. On this project the healthcheck implementation is on charge of `Spring Actuator`. The healthcheck can be reached at this link: [Weather Forecast Health-Check](http://localhost:8080/health) 

##### Performance Test Implementation
`Performance` requirements shall be considered since the initial phases of project, creating a mindset focused on better computacional resources usage, generating a low cost environment, and delivering most value on the user perspective. On this project the performance was measured by JMeter and the computacional resources usage was measured by Java Mission Control. 
If possible, is interesting the creation of a infrastructure to receive and turn easy the visualization of performance gain or losses along the time. It can be done in few hours considering the right technologies, for example, the JMeter would be plotted on a Grafana dashboard consuming from an InfluxDB. populaled by JMeter data being sent by Graphite data protocol. 

##### Maven instead of Gradle or Ant
`Maven` was chosen motivated by excellent way of managing dependencies and plugins integrations. Gradle could be a good alternative.

##### Sonar integration
Similar to Performance Requirement, the technical debit shall be highlighted since the initial phase of a project, that why it was introduced on this one.

##### API Documentation 
The *API documentation* is one of the most important way to communicate with a potentital or common user, that's why it can be neglected. Considering this premisse, it was implemented on this project, and it's on charge of `Swagger`, and this documentation can be found at this link  [Swagger API Docs](http://localhost:8080/swagger-ui.html)

---
### Quality Assurance

##### Load and Performance Test 
* This project is able to handle `8K plus request per second`, considering and cached endpoint. The evidence this capacity is named as *throughput.PNG* .
* Regarding the performance, `with low load` the response time with cache is around `9 ms`, considering a `heavy load (8K per seconds)` the degration is under-control, being able to answer at less than `50 ms`. he evidence this capacity is named as *response-time-over-time.png*.
* Considering the scope of these test it as necessary measure the computational resources usage, with this purpose it was used Java Mission Control and this evidence can was named as *memory-and-cpu-usage-for-8k-requests-per-second.PNG*.     
* Evidences can be found at src/main/resources/qa/evidences. 


