package com.infinitec.weather.bo;

import java.util.List;

import org.json.simple.parser.ParseException;

import com.infinitec.weather.to.DailyWheatherResponse;

/**
 * The Interface WheatherCalculationBO.
 */
public interface WheatherCalculationBO {

	
	/**
	 * Calculate averages.
	 *
	 * @param listInvididualDayResponse the list invididual day response
	 * @return the list
	 * @throws ParseException 
	 */
	public List<DailyWheatherResponse> calculateAverages(List<DailyWheatherResponse> listInvididualDayResponse) throws ParseException;
	
	/**
	 * Populate list.
	 *
	 * @param day the day
	 * @param avgDaily the avg daily
	 * @param avgPressure the avg pressure
	 * @param listDailyWheatherCalculated the list daily wheather calculated
	 * @param dayAveragesResponse the day averages response
	 */
	public void populateList(String day, Double avgDaily, Double avgPressure, List<DailyWheatherResponse> listDailyWheatherCalculated, DailyWheatherResponse dayAveragesResponse);
}
