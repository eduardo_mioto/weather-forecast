package com.infinitec.weather.bo.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;

import com.infinitec.weather.bo.WheatherCalculationBO;
import com.infinitec.weather.commons.TimeHandlerCommons;
import com.infinitec.weather.to.DailyWheatherResponse;

/**
 * The Class WheatherCalculationBOImpl.
 */
@Component
public class WheatherCalculationBOImpl implements WheatherCalculationBO {


	/**
	 * Calculate averages.
	 *
	 * @param listInvididualDayResponse the list invididual day response
	 * @return the list
	 * @throws ParseException 
	 */
	public List<DailyWheatherResponse> calculateAverages(List<DailyWheatherResponse> listInvididualDayResponse) throws ParseException {

		List<DailyWheatherResponse> listdayAveragesCalculated = new ArrayList<DailyWheatherResponse>();
		
		Collections.sort(listInvididualDayResponse);
		
		LocalDate tomorrow = LocalDate.now().plusDays(TimeHandlerCommons.DAYS_TO_ADD_TO_START_TOMORROW);
		LocalDate threeDaysFromNow = tomorrow.plusDays(TimeHandlerCommons.DAYS_TO_ADD_ON_API_QUERY);
		List<LocalDate> localDateList = TimeHandlerCommons.getDatesBetween(tomorrow, threeDaysFromNow);
		
		for (LocalDate desiredLocalDate : localDateList) {
			DailyWheatherResponse dayAveragesResponse = new DailyWheatherResponse();
			
			String desiredDate = desiredLocalDate.format(DateTimeFormatter.ofPattern(TimeHandlerCommons.REGULAR_DATE));
			
			dayAveragesResponse.setDaily(round(averageDaily(listInvididualDayResponse, desiredDate), 2));
			dayAveragesResponse.setNightly(round(averageNightly(listInvididualDayResponse, desiredDate), 2));
			dayAveragesResponse.setPressure(round(averagePressure(listInvididualDayResponse, desiredDate), 2));
			dayAveragesResponse.setDate(desiredDate);
			listdayAveragesCalculated.add(dayAveragesResponse);
		}
		
		Collections.sort(listdayAveragesCalculated);
		return listdayAveragesCalculated;
	}
	
	/**
	 * Round.
	 *
	 * @param value the value
	 * @param places the places
	 * @return the double
	 */
	private static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();
	 
	    BigDecimal bd = new BigDecimal(Double.toString(value));
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}

	/**
	 * Average daily.
	 *
	 * @param listInvididualDayResponse the list invididual day response
	 * @param desiredDate the desired date
	 * @return the double
	 */
	public Double averageDaily(List<DailyWheatherResponse> listInvididualDayResponse, String desiredDate) {
		return listInvididualDayResponse
			    .stream()
			    .filter(p -> p.getDate().equals(desiredDate))
			    .filter(p -> p.getDaily() != null)
			    .mapToDouble(DailyWheatherResponse::getDaily)
			    .average()
			    .getAsDouble();
	}
	
	/**
	 * Average nightly.
	 *
	 * @param listInvididualDayResponse the list invididual day response
	 * @param desiredDate the desired date
	 * @return the double
	 */
	public Double averageNightly(List<DailyWheatherResponse> listInvididualDayResponse, String desiredDate) {
		return listInvididualDayResponse
			    .stream()
			    .filter(p ->  p.getDate().equals(desiredDate))
			    .filter(p -> p.getNightly() != null)
			    .mapToDouble(DailyWheatherResponse::getNightly)
			    .average()
			    .getAsDouble();
	}
	
	/**
	 * Average pressure.
	 *
	 * @param listInvididualDayResponse the list invididual day response
	 * @param desiredDate the desired date
	 * @return the double
	 */
	public Double averagePressure(List<DailyWheatherResponse> listInvididualDayResponse, String desiredDate) {
		return listInvididualDayResponse
			    .stream()
			    .filter(p ->  p.getDate().equals(desiredDate))
			    .filter(p -> p.getPressure() != null)
			    .mapToDouble(DailyWheatherResponse::getPressure)
			    .average()
			    .getAsDouble();
	}
	
	/**
	 * Populate list.
	 *
	 * @param day the day
	 * @param avgDaily the avg daily
	 * @param avgPressure the avg pressure
	 * @param listDailyWheatherCalculated the list daily wheather calculated
	 * @param dayAveragesResponse the day averages response
	 */
	public void populateList(String day, Double avgDaily, Double avgPressure, List<DailyWheatherResponse> listDailyWheatherCalculated, DailyWheatherResponse dayAveragesResponse) {
		dayAveragesResponse.setDaily(avgDaily);
		dayAveragesResponse.setDate(day);
		dayAveragesResponse.setNightly(avgDaily);
		dayAveragesResponse.setPressure(avgPressure);
		listDailyWheatherCalculated.add(dayAveragesResponse);
	}

}
