package com.infinitec.weather.commons;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.json.simple.parser.ParseException;

/**
 * The Class TimeHandlerCommons.
 */
public class TimeHandlerCommons {

	/** The Constant DAYS_TO_ADD_ON_API_QUERY. */
	public static final int 	DAYS_TO_ADD_ON_API_QUERY 		= 3;
	
	/** The Constant DAYS_TO_ADD_TO_START_TOMORROW. */
	public static final int 	DAYS_TO_ADD_TO_START_TOMORROW 	= 1;
	
	/** The Constant HOUR_FORMAT. */
	public static final String  HOUR_FORMAT 				= "HH";
	
	/** The Constant OWM_DATE_TIME_FORMAT. */
	public static final String  OWM_DATE_TIME_FORMAT 	= "yyyy-MM-dd HH:mm:ss";
	
	/** The Constant INFINITEC_DATE. */
	public static final String  REGULAR_DATE 			= "dd/MM/yyyy";
	
	/**
	 * Convert unix datetime.
	 *
	 * @param datetime the datetime
	 * @return the long
	 * @throws ParseException the parse exception
	 */
	public static long convertUnixDatetime(long datetime) throws ParseException {
		return datetime / 1000L;
	}

	/**
	 * Gets the dates between.
	 *
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return the dates between
	 */
	public static List<LocalDate> getDatesBetween(LocalDate startDate, LocalDate endDate) { 
		long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate); 
	    return IntStream.iterate(0, i -> i + 1)
	      .limit(numOfDaysBetween)
	      .mapToObj(i -> startDate.plusDays(i))
	      .collect(Collectors.toList());
	}
	
}
