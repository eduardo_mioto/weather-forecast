package com.infinitec.weather.config;

import java.util.concurrent.TimeUnit;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.cache.CacheBuilder;

/**
 * The Class CacheConfig.
 */
@Configuration
@EnableCaching
public class CacheConfig extends CachingConfigurerSupport {

	/* (non-Javadoc)
	 * @see org.springframework.cache.annotation.CachingConfigurerSupport#cacheManager()
	 */
	@Override
	@Bean
	public CacheManager cacheManager() {
		GuavaCacheManager cacheManager = new GuavaCacheManager();
		return cacheManager;
	}

	/**
	 * Timeout cache manager.
	 *
	 * @return the cache manager
	 */
	@Bean
	public CacheManager timeoutCacheManager() {
		GuavaCacheManager cacheManager = new GuavaCacheManager();
		CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder().expireAfterWrite(4, TimeUnit.HOURS);
		cacheManager.setCacheBuilder(cacheBuilder);
		return cacheManager;
	}

}