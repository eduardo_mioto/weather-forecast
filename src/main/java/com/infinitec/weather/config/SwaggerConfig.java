package com.infinitec.weather.config;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The Class SwaggerConfig.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig { 

	/**
	 * Api.
	 *
	 * @return the docket
	 */
	@Bean
	public Docket api() { 
		return new Docket(DocumentationType.SWAGGER_2)  
				.select()                                  
				.apis(RequestHandlerSelectors.basePackage("com.infinitec.weather.controller"))
				.paths(PathSelectors.any())                          
				.build()
				.apiInfo(apiInfo());
	}

	/**
	 * Api info.
	 *
	 * @return the api info
	 */
	private ApiInfo apiInfo() {
				return new ApiInfo(
						"REST API Wheather Forecast Project", 
						"API focused on Wheather Forecast", 
						"API Term of Service", 
						"Terms of service itself", 
						new Contact("Infinitec Team", "www.infinitec.com", "contact-us@infinitec.com"), 
						"License of API", "API license URL", new ArrayList<>());
	}

}
