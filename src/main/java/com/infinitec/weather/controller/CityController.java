package com.infinitec.weather.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.infinitec.weather.service.CityService;
import com.infinitec.weather.to.DailyWheatherResponse;

import io.undertow.util.BadRequestException;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * The Class CityController.
 */
@EnableSwagger2
@CrossOrigin
@RequestMapping( value = "/city" )
@RestController
public class CityController { 

	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(CityController.class);
	
	/** The base BO impl. */
	@Autowired
	private CityService cityServiceImpl;

	/**
	 * Gets the average by city.
	 *
	 * @param cityName the city name
	 * @param countryUnit the country unit
	 * @return the average by city
	 */
	@RequestMapping( method = RequestMethod.GET, path = "/{cityName}/country/{countryUnit}/average" )
	public ResponseEntity<List<DailyWheatherResponse>> getAverageByCity(@PathVariable ("cityName") String cityName, @PathVariable ("countryUnit") String countryUnit){
		
		log.info("CityController getAverageByCity > cityName: {}, countryUnit:{}", cityName, countryUnit);    	
     	
    	try {
    		List<DailyWheatherResponse> listDailyWheatherAveragesList = cityServiceImpl.getAverageByCity(cityName, countryUnit);
    		return ResponseEntity.accepted().contentType(MediaType.APPLICATION_JSON).body(listDailyWheatherAveragesList);    	
    	} catch (BadRequestException e) {
    		log.error("Error: ", e);
    		return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).build();
    	} catch (Exception e) {
    		log.error("Unexpected Error: ", e);
    		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).build();
    	}
    }

}
