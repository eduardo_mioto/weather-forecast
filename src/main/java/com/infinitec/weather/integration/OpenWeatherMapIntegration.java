package com.infinitec.weather.integration;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.infinitec.weather.commons.TimeHandlerCommons;
import com.infinitec.weather.to.DailyWheatherResponse;

// TODO: Auto-generated Javadoc
/**
 * The Class OpenWeatherMapIntegration.
 */
@Component
public class OpenWeatherMapIntegration {

	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(OpenWeatherMapIntegration.class);
	
	/** The sdf. */
	private SimpleDateFormat sdf = new SimpleDateFormat();
	
	/**
	 * Treat treat Wheather Url
	 * 
	 * Parameters:
	 * q city name and country code divided by comma, use ISO 3166 country codes
	 * type type of the call, keep this parameter in the API call as 'hour'
	 * start start date (unix time, UTC time zone), e.g. start=1369728000
	 * end end date (unix time, UTC time zone), e.g. end=1369789200
	 * cnt amount of returned data (one per hour, can be used instead of 'end')	
	 *
	 * @param city the city name
	 * @param countryCode the countryCode
	 * @param start the start
	 * @param end the end
	 * @return the wheather url
	 */
	public String treatWheatherUrl(String city, String countryCode, long start, long end){
		StringBuilder wheathertUrl = new StringBuilder("http://api.openweathermap.org/data/2.5/forecast?q=");
		wheathertUrl.append(city).append(",").append(countryCode);
		wheathertUrl.append("&type=hour&start=");
		wheathertUrl.append(start).append("&").append("end=").append(end);
		wheathertUrl.append("&units=metric");
		wheathertUrl.append("&appid=73aa1db0a7a8f99f993afb6cede70a00");
		
		log.info("wheathertUrl: {}", wheathertUrl);
		return wheathertUrl.toString();
	}
	
	/**
	 * Extract info from JSON.
	 *
	 * @param response the response
	 * @return the list
	 * @throws ParseException the parse exception
	 * @throws ParseException the parse exception
	 */
	public List<DailyWheatherResponse> extractInfoFromJSON(String response) throws ParseException, java.text.ParseException {

		List<DailyWheatherResponse> listInvididualDayResponse = new ArrayList<DailyWheatherResponse>();
		
		JSONParser parser = new JSONParser(); 
		JSONObject json = (JSONObject) parser.parse(response);
		
		if(json.containsKey("list")) {
			JSONArray list = (JSONArray) json.get("list");

			for (int i = 0; i < list.size(); i++) {
				DailyWheatherResponse resp = new DailyWheatherResponse();
				JSONObject object = (JSONObject) parser.parse(list.get(i).toString());

				log.debug("JSONObject object: {}", object.toJSONString());

				if(object.containsKey("main")) {
					JSONObject mainObject = (JSONObject) object.get("main");
					
					Double temp = getTemp(mainObject);
					Double pressure = getPressure(mainObject);
					Date date = getDate(object);
					
					sdf.applyPattern(TimeHandlerCommons.REGULAR_DATE);
					String day = sdf.format(date);
						
					sdf.applyPattern(TimeHandlerCommons.HOUR_FORMAT);
					Integer hour = Integer.parseInt(sdf.format(date));
					
					if(hour > 6 && hour < 18) {
						resp.setDaily(temp);
					}else {
						resp.setNightly(temp);
					}
					
					resp.setDate(day);
					resp.setPressure(pressure);
					log.debug("day: {} - hour: {} - temp: {} - pressure: {}", day, hour, temp, pressure);
					listInvididualDayResponse.add(resp);
				}
			}
		}
		
		return listInvididualDayResponse;
	}
	
	/**
	 * Gets the temp.
	 *
	 * @param mainObject the main object
	 * @return the temp
	 */
	public Double getTemp(JSONObject mainObject) {
		Double temp = null;
		if(mainObject.containsKey("temp")) {
			temp = checkDoubleFormat(mainObject, "temp", temp);	
		}else {
			log.debug("object temp not found");
		}
		return temp;
	}
	
	/**
	 * Gets the pressure.
	 *
	 * @param mainObject the main object
	 * @return the pressure
	 */
	public Double getPressure(JSONObject mainObject) {
		Double pressure = null;
		if(mainObject.containsKey("pressure")) {
			pressure = checkDoubleFormat(mainObject, "pressure", pressure);			
		}else {
			log.debug("object pressure not found");
		}
		return pressure;
	}

	/**
	 * Check double format.
	 *
	 * @param mainObject the main object
	 * @param objectKey the object key
	 * @param doubleValue the double value
	 * @return the double
	 */
	public Double checkDoubleFormat(JSONObject mainObject, String objectKey, Double doubleValue) {
		Object object = mainObject.get(objectKey);
		if(object instanceof Double) {
			doubleValue =  (Double) object;
		}else if(object instanceof Integer) {
			Integer pressureInt = (Integer) object;
			doubleValue =  Double.valueOf(pressureInt);
		}
		return doubleValue;
	}
	
	/**
	 * Gets the date.
	 *
	 * @param object the object
	 * @return the date
	 * @throws ParseException the parse exception
	 */
	public Date getDate(JSONObject object) throws java.text.ParseException {
		Date date = null;
		if(object.containsKey("dt_txt")) {
			log.debug("object dt_txt: {}", (String) object.get("dt_txt"));

			sdf.applyPattern(TimeHandlerCommons.OWM_DATE_TIME_FORMAT);
			date = sdf.parse((String) object.get("dt_txt"));
		}else {
			log.debug("object dt_txt not found");
		}	
		return date;
	}
}
