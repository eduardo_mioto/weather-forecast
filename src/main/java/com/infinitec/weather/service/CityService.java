package com.infinitec.weather.service;

import java.io.IOException;
import java.util.List;

import org.json.simple.parser.ParseException;

import com.infinitec.weather.to.DailyWheatherResponse;

import io.undertow.util.BadRequestException;

/**
 * The Interface BaseBO.
 */
public interface CityService {

	/**
	 * Gets the average by city.
	 *
	 * @param cityName the city name
	 * @param countryUnit the country unit
	 * @return the average by city
	 * @throws BadRequestException the bad request exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 * @throws ParseException the parse exception
	 */
	public List<DailyWheatherResponse> getAverageByCity(String cityName, String countryUnit) throws BadRequestException, IOException, ParseException, java.text.ParseException;
	
}
