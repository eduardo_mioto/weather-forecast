package com.infinitec.weather.service.impl;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.infinitec.weather.bo.WheatherCalculationBO;
import com.infinitec.weather.commons.HttpCommons;
import com.infinitec.weather.commons.TimeHandlerCommons;
import com.infinitec.weather.integration.OpenWeatherMapIntegration;
import com.infinitec.weather.service.CityService;
import com.infinitec.weather.to.DailyWheatherResponse;

import io.undertow.util.BadRequestException;

/**
 * The Class BaseBOImpl.
 */
@Component
public class CityServiceImpl implements CityService  {

	/** The Constant log. */
	private static final Logger log = LoggerFactory.getLogger(CityServiceImpl.class);

	@Autowired
	private OpenWeatherMapIntegration owmIntegration;
	
	@Autowired
	private WheatherCalculationBO wheatherCalculationBOImpl;
	
	@Cacheable(value="wheatherForecast", cacheManager="timeoutCacheManager")
	public List<DailyWheatherResponse> getAverageByCity(String cityName, String countryUnit) throws BadRequestException, IOException, ParseException, java.text.ParseException {
		if(cityName == null || cityName.isEmpty() || countryUnit == null || countryUnit.isEmpty()) {
			throw new BadRequestException("A Required Parameter was not provided");
		}

		Calendar calendar = Calendar.getInstance();
		long start = TimeHandlerCommons.convertUnixDatetime(calendar.getTimeInMillis());
		calendar.add(Calendar.DATE, TimeHandlerCommons.DAYS_TO_ADD_ON_API_QUERY);
		long end = TimeHandlerCommons.convertUnixDatetime(calendar.getTimeInMillis());

		String response = HttpCommons.callHttp(owmIntegration.treatWheatherUrl(cityName, countryUnit, start, end), "GET");
		log.debug("response: {}", response);

		List<DailyWheatherResponse> listDailyWheatherList = owmIntegration.extractInfoFromJSON(response);
		List<DailyWheatherResponse> listDailyWheatherAveragesList = wheatherCalculationBOImpl.calculateAverages(listDailyWheatherList);

		return listDailyWheatherAveragesList;
	}

	
	


}
