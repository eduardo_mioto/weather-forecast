package com.infinitec.weather.to;

/**
 * The Class DailyWheatherResponse.
 */
public class DailyWheatherResponse implements Comparable<DailyWheatherResponse> { 

	/** The date. */
	private String date;

	/** The daily. */
	private Double daily;

	/** The nightly. */
	private Double nightly;

	/** The pressure. */
	private Double pressure;

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Gets the daily.
	 *
	 * @return the daily
	 */
	public Double getDaily() {
		return daily;
	}

	/**
	 * Sets the daily.
	 *
	 * @param daily the new daily
	 */
	public void setDaily(Double daily) {
		this.daily = daily;
	}

	/**
	 * Gets the nightly.
	 *
	 * @return the nightly
	 */
	public Double getNightly() {
		return nightly;
	}

	/**
	 * Sets the nightly.
	 *
	 * @param nightly the new nightly
	 */
	public void setNightly(Double nightly) {
		this.nightly = nightly;
	}

	/**
	 * Gets the pressure.
	 *
	 * @return the pressure
	 */
	public Double getPressure() {
		return pressure;
	}

	/**
	 * Sets the pressure.
	 *
	 * @param pressure the new pressure
	 */
	public void setPressure(Double pressure) {
		this.pressure = pressure;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((daily == null) ? 0 : daily.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((nightly == null) ? 0 : nightly.hashCode());
		result = prime * result + ((pressure == null) ? 0 : pressure.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DailyWheatherResponse other = (DailyWheatherResponse) obj;
		if (daily == null) {
			if (other.daily != null)
				return false;
		} else if (!daily.equals(other.daily))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (nightly == null) {
			if (other.nightly != null)
				return false;
		} else if (!nightly.equals(other.nightly))
			return false;
		if (pressure == null) {
			if (other.pressure != null)
				return false;
		} else if (!pressure.equals(other.pressure))
			return false;
		return true;
	}

	@Override
	public int compareTo(DailyWheatherResponse o) {
		return getDate().compareTo(o.getDate());
	}
}
