package com.infinitec.weather.commons;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
public class HttpCommonsTest {

	@Test
	public void callHttp() throws IOException {
		String urlToRead = "http://google.com";
		String httpMethod = "GET";
		
		String response = HttpCommons.callHttp(urlToRead, httpMethod);
		assertTrue(response.length() > 0);		
	}
}
