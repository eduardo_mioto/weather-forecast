package com.infinitec.weather.commons;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Date;

import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * The Class TimeHandlerCommons.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class TimeHandlerCommonsTest {
	
	@Test
	public void testConvertUnixDatetime() throws ParseException {
		long datetime = new Date().getTime();
		assertTrue(TimeHandlerCommons.convertUnixDatetime(datetime) ==  datetime / 1000L);
	}
	
	@Test
	public void testGetDatesBetweenTestCase1() throws ParseException {
		
		LocalDate start = LocalDate.now();
		LocalDate end = start.plusDays(2);
		assertTrue(TimeHandlerCommons.getDatesBetween(start, end).size() == 2);
	}
	
	@Test
	public void testGetDatesBetweenTestCase2() throws ParseException {
		
		LocalDate start = LocalDate.now().plusDays(5);
		LocalDate end = start.plusDays(3);
		assertTrue(TimeHandlerCommons.getDatesBetween(start, end).size() == 3);
	}
}
