package com.infinitec.weather.controller;


import static org.mockito.Matchers.any;

import java.io.IOException;
import java.sql.SQLException;

import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.infinitec.weather.service.CityService;

import io.undertow.util.BadRequestException;

@RunWith(MockitoJUnitRunner.class)
public class CityControllerTest {

	@InjectMocks
	private CityController cityController;
	
	@Test 
    public void getAverageByCityTestInternalServerError() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException, BadRequestException, IOException, ParseException, java.text.ParseException {
		
		CityService cityService = Mockito.mock(CityService.class);
		Mockito.when(cityService.getAverageByCity(any(), any())).thenThrow(new IOException());
		
		
		assert(cityController.getAverageByCity(null, "uk").getStatusCode().value() 		== 500);
		assert(cityController.getAverageByCity("london", null).getStatusCode().value() 	== 500);
		assert(cityController.getAverageByCity(null, null).getStatusCode().value() 		== 500);
    }
	
	@Test 
    public void getAverageByCityTestInternalServerError2() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException, BadRequestException, IOException, ParseException, java.text.ParseException {
		
		CityService cityService = Mockito.mock(CityService.class);
		Mockito.when(cityService.getAverageByCity(any(), any())).thenThrow(new ParseException(0));
		
		
		assert(cityController.getAverageByCity(null, "uk").getStatusCode().value() 		== 500);
		assert(cityController.getAverageByCity("london", null).getStatusCode().value() 	== 500);
		assert(cityController.getAverageByCity(null, null).getStatusCode().value() 		== 500);
    }
	
	@Test 
    public void getAverageByCityTestInternalServerError3() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException, BadRequestException, IOException, ParseException, java.text.ParseException {
		
		CityService cityService = Mockito.mock(CityService.class);
		Mockito.when(cityService.getAverageByCity(any(), any())).thenThrow(new java.text.ParseException("", 0));
		
		
		assert(cityController.getAverageByCity(null, "uk").getStatusCode().value() 		== 500);
		assert(cityController.getAverageByCity("london", null).getStatusCode().value() 	== 500);
		assert(cityController.getAverageByCity(null, null).getStatusCode().value() 		== 500);
    }
}
