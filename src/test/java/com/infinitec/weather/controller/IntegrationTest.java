package com.infinitec.weather.controller;


import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import io.undertow.util.BadRequestException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class IntegrationTest {

	@Autowired
	private CityController cityController;
	
	@Test
    public void getAverageByCity() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException, BadRequestException {
        assertTrue(cityController.getAverageByCity("london", "uk") != null);
    }
	
	@Test
    public void getByIdBadRequestException() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException, BadRequestException {
		assert(cityController.getAverageByCity(null, "uk").getStatusCode().value() 		== 400);
		assert(cityController.getAverageByCity("london", null).getStatusCode().value() 	== 400);
		assert(cityController.getAverageByCity(null, null).getStatusCode().value() 		== 400);
    }

}
