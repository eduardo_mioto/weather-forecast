package com.infinitec.weather.to;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class DailyWheatherResponseTest {

	@Test
	public void testEqual() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		DailyWheatherResponse resp2 = new DailyWheatherResponse();
		assertTrue(resp.equals(resp2));
	}
	
	@Test
	public void testEqual2() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		resp.setNightly(1.0);
		resp.setPressure(1.0);
		DailyWheatherResponse resp2 = new DailyWheatherResponse();
		resp2.setNightly(1.0);
		resp2.setPressure(1.0);
		assertTrue(resp.equals(resp2));
	}
	
	@Test
	public void testEqual3() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		resp.setDaily(1.0);
		resp.setDate("");
		resp.setNightly(1.0);
		resp.setPressure(1.0);
		DailyWheatherResponse resp2 = new DailyWheatherResponse();
		resp2.setDaily(1.0);
		resp2.setDate("");
		resp2.setNightly(1.0);
		resp2.setPressure(1.0);
		assertTrue(resp.equals(resp2));
	}
	
	@Test
	public void testEqual4() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		resp.setDaily(1.0);
		resp.setNightly(1.0);
		resp.setPressure(1.0);
		DailyWheatherResponse resp2 = new DailyWheatherResponse();
		resp2.setDaily(1.0);
		resp2.setNightly(1.0);
		resp2.setPressure(1.0);
		assertTrue(resp.equals(resp2));
	}
	
	@Test
	public void testEqual5() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		resp.setNightly(1.0);
		resp.setPressure(1.0);
		DailyWheatherResponse resp2 = new DailyWheatherResponse();
		resp2.setNightly(1.0);
		resp2.setPressure(1.0);
		assertTrue(resp.equals(resp2));
	}
	
	@Test
	public void testNotEqual() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		assertFalse(resp.equals(""));
	}
	
	@Test
	public void testNotEqual2() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		resp.setPressure(1.0);
		DailyWheatherResponse resp2 = new DailyWheatherResponse();
		resp2.setNightly(1.0);
		resp2.setPressure(1.0);
		assertFalse(resp.equals(resp2));
	}
	
	@Test
	public void testNotEqual3() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		resp.setDaily(1.0);
		resp.setNightly(1.0);
		resp.setPressure(1.0);
		DailyWheatherResponse resp2 = new DailyWheatherResponse();
		resp2.setDaily(1.0);
		resp2.setDate("");
		resp2.setNightly(1.0);
		resp2.setPressure(1.0);
		assertFalse(resp.equals(resp2));
	}
	
	@Test
	public void testNotEqual4() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		resp.setDaily(1.0);
		resp.setPressure(1.0);
		DailyWheatherResponse resp2 = new DailyWheatherResponse();
		resp2.setDaily(1.0);
		resp2.setNightly(1.0);
		resp2.setPressure(1.0);
		assertFalse(resp.equals(resp2));
	}
	
	@Test
	public void testNotEqual5() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		resp.setNightly(1.0);
		DailyWheatherResponse resp2 = new DailyWheatherResponse();
		resp2.setNightly(1.0);
		resp2.setPressure(1.0);
		assertFalse(resp.equals(resp2));
	}
	
	@Test
	public void testHasCode() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		assertTrue(resp.hashCode() == 923521);
	}
	
	@Test
	public void testHasCode2() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		resp.setDaily(1.0);
		assertFalse(resp.hashCode() == 923521);
	}
	
	@Test
	public void testHasCode4() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		resp.setDate("teste");
		assertFalse(resp.hashCode() == 923521);
	}
	
	@Test
	public void testHasCode5() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		resp.setNightly(1.0);
		assertFalse(resp.hashCode() == 923521);
	}
	
	@Test
	public void testHasCode6() {
		DailyWheatherResponse resp = new DailyWheatherResponse();
		resp.setPressure(1.0);
		assertFalse(resp.hashCode() == 923521);
	}
	
}
